package usecases

import (
	"context"

	"codebase-go/bin/modules/inventory"
	"codebase-go/bin/modules/inventory/models"
	"codebase-go/bin/pkg/errors"
	"codebase-go/bin/pkg/redis"
	"codebase-go/bin/pkg/utils"
)

type queryUsecase struct {
	inventoryRepositoryQuery inventory.MongodbRepositoryQuery
}

func NewQueryUsecase(mq inventory.MongodbRepositoryQuery, rc redis.Collections) inventory.UsecaseQuery {
	return &queryUsecase{
		inventoryRepositoryQuery: mq,
	}
}

func (q queryUsecase) GetItem(ctx context.Context, kode string) utils.Result {
	var result utils.Result

	queryRes := <-q.inventoryRepositoryQuery.FindOne(ctx, kode)

	if queryRes.Error != nil {
		errObj := errors.InternalServerError("Internal server error")
		result.Error = errObj
		return result
	}
	item := queryRes.Data.(models.Inventory)
	res := models.Inventory{
		Id:           item.Id,
		Nama:     item.Nama,
		Kode:        item.Kode,
		Harga:     item.Harga,
		Stock: item.Stock,
		Kategori:       item.Kategori,
	}
	result.Data = res
	return result
}
